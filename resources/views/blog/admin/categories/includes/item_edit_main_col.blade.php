@php
    /** @var \App\Models\BlogCategory $item */
    /** @var \Illuminate\Support\Collection $categoryList */
@endphp
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="card-title"></div>
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#maindata" role="tab">Основные данные</a>
                    </li>
                </ul>
                <br>
                <div class="tab-content">
                    <div class="tab-pane active" id="maindata" role="tabpanel">
                        <div class="mb-3">
                            <label for="title" class="form-label">Заголовок</label>
                            <input name="title" value="{{ $item->title }}"
                                   id="title"
                                   type="text"
                                   class="form-control"
                                   minlength="3">
                        </div>
                        <div class="mb-3">
                            <label for="slug" class="form-label">Идентификатор</label>
                            <input name="slug" value="{{ $item->slug }}"
                                   id="slug"
                                   type="text"
                                   class="form-control"
                                   minlength="3">
                        </div>
                        <div class="mb-3">
                            <label for="parent_id" class="form-label">Родитель</label>
                            <select name="parent_id"
                                    id="parent_id"
                                    class="form-select">
                                @foreach($categoryList as $categoryOption)
                                    <option value="{{ $categoryOption->id }}"
                                            @if($categoryOption->id == $item->parent_id) selected @endif>
                                        {{ $categoryOption->id_title }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-floating">
                            <textarea name="description"
                                      id="description"
                                      class="form-control"
                                      rows="3"
                                      style="height: 100px">{{ old('description', $item->description) }}</textarea>
                            <label for="description">Описание</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
