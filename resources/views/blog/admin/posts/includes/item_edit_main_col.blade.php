@php
    /** @var \App\Models\BlogPost $item */
@endphp
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                @if($item->is_published)
                    Опубликовано
                @else
                    Черновик
                @endif
            </div>
            <div class="card-body">
                <div class="card-title"></div>
                <ul class="nav nav-tabs" id="nav-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="nav-home-tab" data-bs-toggle="tab" href="#nav-main" role="tab" aria-controls="nav-home" aria-selected="true">Основные данные</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="nav-profile-tab" data-bs-toggle="tab" href="#nav-add" role="tab" aria-controls="nav-profile" aria-selected="false">Доп. данные</a>
                    </li>
                </ul>
                <br>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="nav-main" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="mb-3">
                            <label for="title" class="form-label">Заголовок</label>
                            <input name="title" value="{{ $item->title }}"
                                   id="title"
                                   type="text"
                                   class="form-control"
                                   minlength="3">
                        </div>
                        <div class="form-floating">
                            <textarea name="content_raw"
                                      id="content_raw"
                                      class="form-control"
                                      rows="20" style="min-height: 300px">{{ old('content_raw', $item->content_raw) }}</textarea>
                            <label for="content_raw">Cтатья</label>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="nav-add" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <div class="mb-3">
                            <label for="category_id" class="form-label">Родитель</label>
                            <select name="category_id"
                                    id="category_id"
                                    class="form-select">
                                @foreach($categoryList as $categoryOption)
                                    <option value="{{ $categoryOption->id }}"
                                            @if($categoryOption->id == $item->category_id) selected @endif>
                                        {{ $categoryOption->id_title }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="slug" class="form-label">Идентификатор</label>
                            <input name="slug" value="{{ $item->slug }}"
                                   id="slug"
                                   type="text"
                                   class="form-control"
                                   minlength="3">
                        </div>
                        <div class="form-floating mb-3">
                            <textarea name="excerpt"
                                      id="excerpt"
                                      class="form-control"
                                      rows="3" style="min-height: 100px">{{ old('excerpt', $item->excerpt) }}</textarea>
                            <label for="content_raw">Выдержка</label>
                        </div>
                        <div class="form-check">
                            <input name="is_published"
                                   type="hidden"
                                   value="0">

                            <input name="is_published"
                                   type="checkbox"
                                   class="form-check-input"
                                   value="1"
                                   @if($item->is_published)
                                   checked
                                   @endif
                                   id="is_published">
                            <label class="form-check-label" for="is_published">
                                Опубликовано
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
